#include <stdio.h>

int add(int, int);
int analyze(char*);
int operate(char, int, int);


int main() {
    char input[10];
    printf("Welcome to mat's calculator 0.0.0.1\n");
    fgets(input, 10, stdin);

    printf("Input is:  %s", input);

    printf("%d\n", analyze(input));
}

int add(int x, int y) {
    return x + y;
}

int analyze(char s[]){
    int i, operated;
    char operator;
    int num1 = 0; 
    int num2 = 0;
    for(i = 0; i < 10; ++i){
        if(s[i] == '+'){
            operator = '+';
            operated = 1;
        } else if (s[i] == ' ' || s[i] == '\n' || s[i] == '\0') {
            printf("char is %c\n", s[i]);
        } else if (s[i] >= '0' || s[i] <= '9'){
            if (operated == 1) {
                printf("num2 num is %c\n", s[i]);
                num2 = 10 * num2 + (s[i] - '0');
                printf("num2 = %d\n", num2); 
            } else {
                printf("num1 num is %c\n", s[i]);   
                num1 = 10 * num1 + (s[i] - '0');
                printf("num1 = %d\n", num1); 
            }

        }
    }
    return operate(operator, num1, num2);
}

int operate(char op, int v, int z) {
    switch(op){
        case '+':
            return add(v, z);
        default:
            return -1;
    }
}
